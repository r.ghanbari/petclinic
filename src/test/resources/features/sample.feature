@sample_annotation
Feature: Sample Feature

  Background: Sample General Preconditions Explanation
    Given There is some predefined pet types like "dog"

  Scenario: First Scenario Name
    Given There is a pet owner called "Amu Gholam" "Gholami"
    When He performs save pet service to add a pet to his list
    Then The pet is saved successfully

  Scenario:
    Given There is a pet owner called "Amu Gholam" "Gholami" and he has a pet by id 1
    When He performs find pet service to find a pet by id 1
    Then The pet with id 1 will be found successfully

  Scenario:
    Given There is a owner called "Amu Gholam" "Gholami" and id 1
    When Someone wants to find an owner by id 1
    Then The owner with name "Amu Gholam" "Gholami" will be found successfully

  Scenario:
    Given There is a pet owner called "Amu Gholam" "Gholami"
    When he performs new pet service to create a new pet
    Then a new pet will be created with that owner
