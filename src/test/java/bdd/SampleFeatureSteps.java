package bdd;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.owner.*;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class SampleFeatureSteps {

	@Autowired
	PetService petService;

	@Autowired
	OwnerRepository ownerRepository;

	@Autowired
	PetRepository petRepository;

	@Autowired
	PetTypeRepository petTypeRepository;

	private Owner gholam;
	private Owner foundedOwner;
	private PetType petType;
	private Pet aPet;

	@Before("@sample_annotation")
	public void setup() {
		// sample setup code
	}

	@Given("There is a pet owner called {string} {string}")
	public void thereIsAPetOwnerCalled(String name, String family) {
		gholam = new Owner();
		gholam.setFirstName(name);
		gholam.setLastName(family);
		gholam.setAddress("Najibie - Kooche shahid abbas alavi");
		gholam.setCity("Tehran");
		gholam.setTelephone("09191919223");
		ownerRepository.save(gholam);
	}

	@When("He performs save pet service to add a pet to his list")
	public void hePerformsSavePetService() {
		Pet pet = new Pet();
		pet.setType(petType);
		pet.setName("petty");
		pet.setBirthDate(LocalDate.now());
		petService.savePet(pet, gholam);
	}

	@Then("The pet is saved successfully")
	public void petIsSaved() {
		assertNotNull(petService.findPet(petType.getId()));
	}

	@Given("There is some predefined pet types like {string}")
	public void thereIsSomePredefinedPetTypesLike(String petTypeName) {
		petType = new PetType();
		petType.setName(petTypeName);
		petTypeRepository.save(petType);
	}

	@Given("There is a pet owner called {string} {string} and he has a pet by id {int}")
	public void thereIsAPetOwnerCalledAndHeHasAPetById(String name, String family, int id) {
		gholam = new Owner();
		gholam.setFirstName(name);
		gholam.setLastName(family);
		gholam.setAddress("Najibie - Kooche shahid abbas alavi");
		gholam.setCity("Tehran");
		gholam.setTelephone("09191919223");
		ownerRepository.save(gholam);
		Pet pet = new Pet();
		pet.setName("aPet");
		pet.setType(petType);
		pet.setBirthDate(LocalDate.now());
		pet.setId(id);
		aPet = pet;
		petService.savePet(aPet, gholam);
	}

	@When("He performs find pet service to find a pet by id {int}")
	public void hePerformsFindPetServiceToFindAPetById(int arg0) {
		this.aPet = petService.findPet(arg0);
	}

	@Then("The pet with id {int} will be found successfully")
	public void thePetWithIdWillBeFoundSuccessfully(int arg0) {
		assertEquals(arg0, this.aPet.getId());
	}

	@Given("There is a owner called {string} {string} and id {int}")
	public void thereIsAOwnerCalledAndId(String arg0, String arg1, int arg2) {
		gholam = new Owner();
		gholam.setId(arg2);
		gholam.setFirstName(arg0);
		gholam.setLastName(arg1);
		gholam.setAddress("Najibie - Kooche shahid abbas alavi");
		gholam.setCity("Tehran");
		gholam.setTelephone("09191919223");
		ownerRepository.save(gholam);
	}

	@When("Someone wants to find an owner by id {int}")
	public void someoneWantsToFindAnOwnerById(int arg0) {
		this.foundedOwner = this.petService.findOwner(arg0);
	}

	@Then("The owner with name {string} {string} will be found successfully")
	public void theOwnerWithNameWillBeFoundSuccessfully(String arg0, String arg1) {
		assertEquals(arg0, foundedOwner.getFirstName());
		assertEquals(arg1, foundedOwner.getLastName());
	}

	@When("he performs new pet service to create a new pet")
	public void hePerformsNewPetServiceToCreateANewPet() {
		this.aPet = petService.newPet(gholam);
	}

	@Then("a new pet will be created with that owner")
	public void aNewPetWillBeCreatedWithThatOwner() {
		assertEquals(aPet.getOwner(), gholam);
	}
}
